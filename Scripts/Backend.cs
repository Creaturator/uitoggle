using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

namespace UIToggle.Scripts {
    public class Backend{
        
        private static Backend instance;

        private const string successMessage = "Sucessfully toggled visibility.";
        
        public static Backend getInstance() => instance ?? (instance = new Backend());

        private Backend() {}

        public C[] getAllChildsWithComponent<C>(GameObject parent) where C : Component =>
            getAllChildsWithComponent<C>(parent.transform);
        
        public C[] getAllChildsWithComponentDelParent<C>(GameObject parent) where C : Component {
            List<C> childs = getAllChildsWithComponent<C>(parent.transform).ToList();
            childs.RemoveAt(0);
            return childs.ToArray();
        }
        
        public C[] getAllChildsWithComponent<C>(Transform parent) where C : Component {
            List<C> list = new List<C>();
            list = getChildsRek(ref list, parent);
            return list.ToArray();
        }
        
        private ref List<C> getChildsRek<C>(ref List<C> former, Transform obj) where C : Component{
            if (obj.TryGetComponent(out C comp))
                former.Add(comp);
            for (int i = 0; i < obj.childCount; i++)
                getChildsRek(ref former, obj.GetChild(i));
            return ref former;
        }

        public void toggleActivation() {
            Frontend frontInst = Frontend.instance;
            if (frontInst.input == null) {
                frontInst.result.text = "An Error occured. The input field is Null.";
                return;
            }
            
            const string errorNoObject = "Please select a UI object before starting.";
            if (frontInst.input.value == null) {
                frontInst.result.text = errorNoObject;
                return;
            }
            RectTransform parent = frontInst.input.value as RectTransform;
            if (parent == null) {
                frontInst.result.text = errorNoObject;
                return;
            }
            
            Image[] pictures = this.getAllChildsWithComponent<Image>(parent);
            Text[] texts =  this.getAllChildsWithComponent<Text>(parent);
            if (parent == null || (pictures.Length <= 0 && texts.Length <= 0)) {
                frontInst.result.text = errorNoObject;
                return;
            }
            foreach(Image objImg in pictures) 
                objImg.enabled = !objImg.enabled;
            foreach(Text objTxt in texts) 
                objTxt.enabled = !objTxt.enabled;
            
            frontInst.result.text = successMessage;
        }
    }
}
