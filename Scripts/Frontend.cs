//Reference: UnityEngine.UI

using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Button = UnityEngine.UIElements.Button;
using RectTransform = UnityEngine.RectTransform;

namespace UIToggle.Scripts {
    public class Frontend : EditorWindow {

        public Button startButton {get; private set;}
        public ObjectField input {get; private set;}
        public VisualElement root {get; private set;}
        public Label result {get; private set;}
        
        public static Frontend instance {get; private set;}

        private Frontend() {}
        
        [MenuItem("GameObject/UIToggle")]
        public static void ShowExample() {
            instance = GetWindow<Frontend>();
            instance.titleContent = new GUIContent("UIToggle");
        }

        public void CreateGUI() {

            // Import UXML
            string ort = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (ort == null) {
                Debug.LogError("UXML File could not be found.");
                return;
            }
            ort = ort.Substring(ort.IndexOf("Assets", StringComparison.CurrentCulture));
            VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(ort + "\\UI.uxml");
            if (visualTree == null) {
                Debug.LogError("UXML File could not be found.");
                return;
            }
            VisualElement labelFromUXML = visualTree.Instantiate();
            rootVisualElement.Add(labelFromUXML);

            root = rootVisualElement.Q<VisualElement>("Root");
            
            startButton = root.Q<Button>("BeginToggle");
            startButton.clicked += Backend.getInstance().toggleActivation;
            
            result = root.Q<Label>("Result");
            VisualElement inputDummy = root.Q<VisualElement>("InputFeld");
            input = new ObjectField("Parent Game Object") {objectType = typeof(RectTransform), allowSceneObjects = true, name = inputDummy.name, tooltip = inputDummy.tooltip};
            inputDummy.Add(input);
            inputDummy.tooltip = String.Empty;
            Vector3 dummyPos = inputDummy.transform.position;
            input.transform.position = new Vector3(dummyPos.x / 2, dummyPos.y / 2, dummyPos.z / 2);
            input.transform.scale = inputDummy.transform.scale;
        }
    }
}