Welcome to the Readme for my Unity Plugin: The UIToggle.
The purpose of the plugin is very simple: When you have your InGame UI and you want to reposition/scale/rotate a normally disabled object,you can just start the UIToggle, but in the object you want to change the visiblity of and then the Plugin will enable the image and the text Components of the input object and all of its children. In Short: The object and all oif its children will be made visible.
Then you can work with them and after finished, you simply do the same in the UIToggle to hide the object you were working on.
If you want to change multiple objects, make a new empty GameObject, put every obejct you want to change the visbility of, as a child and pass that parent to the plugin.

Long story short: With the UIToggle you no longer need to to through every object and en- or disable the visual Components. You only need to put in the object you want to toggle the visibility of and the plugin will do the rest for you.

Node: This Plugin is hardcoded to just change Image and Text Components of UI Gameobjects from the UnityEngine.UI Package.

The Sourcecode can be found in my Gitlab Repository: https://gitlab.com/Creaturator/uitoggle